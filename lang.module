<?php

/**
 * @file
 * Defines simple language field type.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Unicode;

/**
 * Implements hook_help().
 */
function lang_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.lang':
      $output = '';
      $output .= '<h3>' . t('Simple language field') . '</h3>';
      $output .= '<p>' . t('The Lang module defines a simple language field type for the Field module. It provides 2 widgets - select options and autocomplete textfield - for this purpose.  See the <a href="!field-help">Field module help page</a> for more information about fields.', array('!field-help' => \Drupal::url('help.page.field_ui'))) . '</p>';
      return $output;
  }
}

/**
 * Form element validate handler for language autocomplete element.
 */
function lang_autocomplete_validate($element, FormStateInterface $form_state) {
  if ($language = $element['#value']) {
    $languages = getLanguageOptions();
    $langcode = array_search($language, $languages);
    if (!empty($langcode)) {
      $form_state->setValueForElement($element, $langcode);
    }
  }
}

/**
 * Get all core languages.
 *
 * @param string $type
 * @return array
 */
function getLanguageOptions($type = 'en') {
  $select_options = array();
  $standard_languages = LanguageManager::getStandardLanguageList();
  foreach ($standard_languages as $langcode => $language_names) {
    $language_name = '';
    switch ($type) {
      case 'en':
        $language_name .= $language_names[0];
        break;
      case 'loc':
        $language_name .= $language_names[1];
        break;
      case 'both':
        $language_name .= $language_names[0];
        if (Unicode::strlen($language_names[1])) {
          $language_name .= ' (' . $language_names[1] . ')';
        }
        break;
    }
    $select_options[$langcode] = $language_name;
  }
  return $select_options;
}
